pragma solidity ^0.4.0;

contract CustodialContract {

  address client;
  bool _switch = false;

  //With event
  event updateStatus(string _msg, uint amount);
  event updateUser(string _msg, address _user, uint amount);
  function CustodialContract()
  {
    client = msg.sender;
  }

  modifier ifClient() {
    if(msg.sender!=client)
    {
      throw;
    }
    _;

  }
  function depositFunds() payable{
    updateStatus("user has deposited money", msg.value);
    updateUser("user has transferred", msg.sender, msg.value);
  }

  function withdrawFunds(uint amount) ifClient {
    if(client.send(amount))
    {
      updateStatus("user has deposited money", 0);
      updateUser("user has transferred", msg.sender, 0);
      _switch = true;
    }
  }

  function getFunds() ifClient constant returns (uint)
  {
    return this.balance;
  }

}
