pragma solidity ^0.4.0;

contract HelloWorldContract {

  string word = "hello world";
  address public issuer;

  function HelloWorldContract()
  {
    issuer = msg.sender;
  }
  modifier ifIssuer()
  {
    if(issuer!=msg.sender)
    {
      throw;
    }
    _;
  }

  function getWord() constant returns (string)
  {
    return word;
  }

  function setWord(string newWord) ifIssuer returns (string) {
    word = newWord;
    return word;
  }
}
//Output code: https://ethfiddle.com/56aZZLJcbL
