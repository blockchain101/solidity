pragma solidity ^0.4.0;

contract Fundraiser {

  struct Funder{
    address funderAddr;
    uint amount;
  }

  struct Campaign
  {
    address beneficiary;
    uint goalAmount;
    uint funds;
    uint noOfFunders;
    mapping (uint => Funder) funders;
  }

  uint numCampaigns = 0;
  mapping (uint => Campaign) campaigns;

  function newCampaign(address beneficiary, uint goal) returns (uint campaignID)
  {
    campaignID = numCampaigns++;
    campaigns[campaignID] = Campaign(beneficiary, goal, 0,0);
  }

  function contribute(uint campaignID) payable {
    Campaign storage c =  campaigns[campaignID];
    c.funders[c.noOfFunders++] = Funder({funderAddr: msg.sender, amount: msg.value});
    c.funds += msg.value;
  }

  function checkGoalReached(uint campaignID) constant returns (bool reached)
  {
    Campaign storage c = campaigns[campaignID];
    if(c.funds < c.goalAmount)
    {
      return false;
    }
    uint amount = c.funds;
    c.funds = 0;
    c.beneficiary.transfer(amount);
    return true;
  }
}
