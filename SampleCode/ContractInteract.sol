pragma solidity ^0.4.0;


contract callerContract {

 calledContract tobeCalled  = new calledContract();
 // or calledContract tobeCalled = calledContract(address of previous contract to fetch the latest value)

 function getNumber() constant returns (uint)
 {
   return tobeCalled.getNumber();
 }

 function getWord() constant returns (bytes32)
 {
   return tobeCalled.getWord();
 }


}

contract calledContract {

  uint num = 22;
  bytes32 word = "hello world";

  function getNumber() constant returns (uint)
  {
    return num;
  }

  function getWord() constant returns (bytes32)
  {
    return word;
  }

}
